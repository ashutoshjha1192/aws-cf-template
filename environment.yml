AWSTemplateFormatVersion: '2010-09-09'
Description: 'VPC, Subnets, Security Group for LKQ VMN'

Parameters:

  VpcBlock:
    Type: String
    Default: 10.1.0.0/16
    Description: The CIDR range for the VPC. This should be a valid private (RFC 1918) CIDR range.

  Subnet01Block:
    Type: String
    Default: 10.1.0.0/24
    Description: CidrBlock for subnet 01 within the VPC

  Subnet02Block:
    Type: String
    Default: 10.1.1.0/24
    Description: CidrBlock for subnet 02 within the VPC

  Subnet03Block:
    Type: String
    Default: 10.1.2.0/24
    Description: CidrBlock for subnet 03 within the VPC
  env:
    Type: String
    AllowedValues: 
      - prod
      - qa
      - test
      - dev
    Description: Environment Name

Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
      -
        Label:
          default: "Network Configuration"
        Parameters:
          - VpcBlock
          - Subnet01Block
          - Subnet02Block
          - Subnet03Block

Resources:
  VPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock:  !Ref VpcBlock
      EnableDnsSupport: true
      EnableDnsHostnames: true
      Tags:
        - 
          Key: Name
          Value: !Sub 'mvn-${env}-vpc'
        - 
          Key: Environment
          Value: !Ref env

  InternetGateway:
    Type: "AWS::EC2::InternetGateway"
    Properties:
      Tags:
        - 
          Key: Name
          Value: !Sub 'mvn-${env}-ig'
        - 
          Key: Environment
          Value: !Ref env

  VPCGatewayAttachment:
    Type: "AWS::EC2::VPCGatewayAttachment"
    Properties:
      InternetGatewayId: !Ref InternetGateway
      VpcId: !Ref VPC

  RouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
      Tags:
        - 
          Key: Name
          Value: !Sub 'mvn-${env}-public-rt'
        - 
          Key: Environment
          Value: !Ref env

  Route:
    DependsOn: VPCGatewayAttachment
    Type: AWS::EC2::Route
    Properties:
      RouteTableId: !Ref RouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway

  Subnet01:
    Type: AWS::EC2::Subnet
    Metadata:
      Comment: Subnet 01
    Properties:
      AvailabilityZone:
        Fn::Select:
        - '0'
        - Fn::GetAZs:
            Ref: AWS::Region
      CidrBlock:
        Ref: Subnet01Block
      VpcId:
        Ref: VPC
      Tags:
        - 
          Key: Name
          Value: !Sub "mvn-${env}-subnet01-public"
        - 
          Key: Environment
          Value: !Ref env

  Subnet02:
    Type: AWS::EC2::Subnet
    Metadata:
      Comment: Subnet 02
    Properties:
      AvailabilityZone:
        Fn::Select:
        - '1'
        - Fn::GetAZs:
            Ref: AWS::Region
      CidrBlock:
        Ref: Subnet02Block
      VpcId:
        Ref: VPC
      Tags:
        - 
          Key: Name
          Value: !Sub "mvn-${env}-subnet02-public"
        - 
          Key: Environment
          Value: !Ref env
      
  Subnet03:
    Type: AWS::EC2::Subnet
    Metadata:
      Comment: Subnet 03
    Properties:
      AvailabilityZone:
        Fn::Select:
        - '2'
        - Fn::GetAZs:
            Ref: AWS::Region
      CidrBlock:
        Ref: Subnet03Block
      VpcId:
        Ref: VPC
      Tags:
        - 
          Key: Name
          Value: !Sub "mvn-${env}-subnet03-public"
        - 
          Key: Environment
          Value: !Ref env

  Subnet01RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref Subnet01
      RouteTableId: !Ref RouteTable

  Subnet02RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref Subnet02
      RouteTableId: !Ref RouteTable 

  Subnet03RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref Subnet03
      RouteTableId: !Ref RouteTable

  SecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Security group for public subnets
      VpcId: !Ref VPC
      Tags:
        - 
          Key: Name
          Value: !Sub "mvn-${env}-sg"
        - 
          Key: Environment
          Value: !Ref env
  SecurityIngress:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      GroupId: !Ref SecurityGroup
      IpProtocol: -1
      FromPort: 0
      ToPort: 65535
      CidrIp: !Ref VpcBlock

  LoadBalancer:
    Type: 'AWS::ElasticLoadBalancingV2::LoadBalancer'
    Properties:
      LoadBalancerAttributes:
        - 
          Key: load_balancing.cross_zone.enabled
          Value: True
      
      Type: network
      Name: !Join ['', [mvn,-, lb-, !Ref env]]
      Scheme: internal
      Subnets:
        - !Ref Subnet01
        - !Ref Subnet02
        - !Ref Subnet03
      Tags:
        - 
          Key: Environment
          Value: !Ref env
  Cluster:
    Type: 'AWS::ECS::Cluster'
    Properties:
      ClusterName:  !Sub "mvn-${env}-fargate-cluster"
      Tags:
        - 
          Key: Environment
          Value: !Ref env
  LogGroup:
    Type: 'AWS::Logs::LogGroup'
    Properties:
      LogGroupName: !Join ['', [/ecs/, mvn/, !Ref env]]

Outputs:
 
  VPC:
    Description: 'VPC'
    Value: !Ref VPC
    Export:
      Name: !Sub 'mvn-${env}-vpc'

  Subnet01:
    Description: 'Public Subnet 01'
    Value: !Ref Subnet01
    Export:
      Name: !Sub 'mvn-${env}-subnet01-public'

  Subnet02:
    Description: 'Public Subnet 02'
    Value: !Ref Subnet02
    Export:
      Name: !Sub 'mvn-${env}-subnet02-public'

  Subnet03:
    Description: 'Public Subnet 03'
    Value: !Ref Subnet03
    Export:
      Name: !Sub 'mvn-${env}-subnet03-public'
      
  SecurityGroup:
    Description: Security group for public subnets
    Value: !Ref SecurityGroup
    Export:
      Name: !Sub 'mvn-${env}-sg'
  Cluster:
    Description: Cluster
    Value: !Ref Cluster
    Export:
      Name: !Sub "mvn-${env}-fargate-cluster" 
  LoadBalancerARN:
    Description: ARN Of Load balancer
    Value:  !Sub '${LoadBalancer}'
    Export:
      Name: !Sub "mvn-${env}-nlb-internal-arn"

